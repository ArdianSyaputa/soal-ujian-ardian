import React from "react";
import Clock from "react-live-clock";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import "../Css/Sidebar.css";
import { AiTwotoneHome } from "react-icons/ai";
import { FaUserGraduate } from "react-icons/fa";
import { FaUserTie } from "react-icons/fa";
import { ImManWoman } from "react-icons/im";
import { BiDonateHeart } from "react-icons/bi";
import { RiFocus3Fill } from "react-icons/ri";
import { ImJoomla } from "react-icons/im";
import { ImExit } from "react-icons/im";
import { FaCapsules } from "react-icons/fa";
import { FaUser } from "react-icons/fa";
import { MdVerifiedUser } from "react-icons/md";
export default function Sidebar() {
  const history = useHistory();
  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
        //Untuk munuju page selanjutnya
        history.push("/");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        localStorage.clear();
      }
    });
  };

  var mini = true;

  const [isOpen, setIsOpen] = useState(false);
  const [isOpenSiswa, setIsOpenSiswa] = useState(false);
  const [isOpenMapel, setIsOpenMapel] = useState(false);
  const [isOpenLaporan, setIsOpenLaporan] = useState(false);
  return (
    <div class="sidebar h-screen top-0 bottom-0 lg:left-0 p-2 w-[300px] overflow-y-auto text-center bg-gray-900">
      <div class="text-gray-100 text-xl">
        <div class="p-2.5 mt-1 flex items-center">
          <h1 class="font-bold text-gray-200 text-[15px] ml-3">
            SISTEM APLIKASI UKS SMK BINANUSANTARA
          </h1>
        </div>
        <div class="my-2 bg-gray-600 h-[1px]"></div>
      </div>
      <a href="/Dashboard">
        <div class="p-2.5 mt-3 flex items-center rounded-md px-4 duration-300 cursor-pointer hover:bg-green-600 text-white">
          <AiTwotoneHome className="text-xl" />
          <span class="text-[15px] ml-4 text-gray-200 font-bold">
            Dashboard
          </span>
        </div>
      </a>
      <a href="/Periksa">
        <div class="p-2.5 mt-3 flex items-center rounded-md px-4 duration-300 cursor-pointer hover:bg-green-600 text-white">
          <MdVerifiedUser className="text-xl" />
          <span class="text-[15px] ml-4 text-gray-200 font-bold">
            Periksa Pasien
          </span>
        </div>
      </a>
      <details class="group [&_summary::-webkit-details-marker]:hidden">
        <summary class="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-gray-100 hover:bg-green-600 hover:text-gray-700">
          <div class="flex items-center gap-2">
            <FaUser className="text-xl" />
            <span class="text-sm font-medium"> Data </span>
          </div>

          <span class="shrink-0 transition duration-300 group-open:-rotate-180">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                clip-rule="evenodd"
              />
            </svg>
          </span>
        </summary>

        <nav aria-label="Account Nav" class="mt-2 flex flex-col px-4">
          <a
            href="/DaftarGuru"
            class="flex items-center gap-2 rounded-lg px-4 py-2 text-gray-100 hover:bg-green-600 hover:text-gray-700"
          >
            <FaUserGraduate />
            <span class="text-sm font-medium"> Daftar Guru </span>
          </a>

          <a
            href="DaftarSiswa"
            class="flex items-center gap-2 rounded-lg px-4 py-2 text-gray-100 hover:bg-green-600 hover:text-gray-700"
          >
            <ImManWoman />
            <span class="text-sm font-medium"> Daftar Siswa </span>
          </a>

          <form action="/DaftarKaryawan">
            <button
              type="submit"
              class="flex w-full items-center gap-2 rounded-lg px-4 py-2 text-gray-100 hover:bg-green-600 hover:text-gray-700"
            >
              <FaUserTie />
              <span class="text-sm font-medium"> Daftar Karyawan </span>
            </button>
          </form>
        </nav>
      </details>
      <a href="/Diagnosa">
        <div class="p-2.5 mt-3 flex items-center rounded-md px-4 duration-300 cursor-pointer hover:bg-green-600 text-white">
          <BiDonateHeart className="text-xl" />
          <span class="text-[15px] ml-4 text-gray-200 font-bold">Diagnosa</span>
        </div>
      </a>
      <a href="/Penanganan">
        <div class="p-2.5 mt-3 flex items-center rounded-md px-4 duration-300 cursor-pointer hover:bg-green-600 text-white">
          <ImJoomla className="text-xl" />
          <span class="text-[15px] ml-4 text-gray-200 font-bold">
            Penanganan Pertama
          </span>
        </div>
      </a>
      <a href="/Tindakan">
        <div class="p-2.5 mt-3 flex items-center rounded-md px-4 duration-300 cursor-pointer hover:bg-green-600 text-white">
          <RiFocus3Fill className="text-xl" />
          <span class="text-[15px] ml-4 text-gray-200 font-bold">Tindakan</span>
        </div>
      </a>
      <a href="/DaftarObat">
        <div class="p-2.5 mt-3 flex items-center rounded-md px-4 duration-300 cursor-pointer hover:bg-green-600 text-white">
          <FaCapsules className="text-xl" />
          <span class="text-[15px] ml-4 text-gray-200 font-bold">
            Daftar Obat P3K
          </span>
        </div>
      </a>
      <div class="my-4 bg-gray-600 h-[1px]"></div>
      <h1 className="text-[15px] ml-4 text-gray-200 font-bold">
        <Clock format={"HH:mm:ss"} ticking={true} />
      </h1>
      <a
        href="#"
        class="flex items-center gap-2 bg-gray-900 p-4 hover:bg-gray-600"
      >
        <img
          alt=""
          src="https://apps.owibu.com/berita/banner/1605682455author-majo-no-tabitabi-elaina-berpetualang-bukan-untuk-memperbaiki-dunia.jpg"
          class="h-10 w-10 rounded-full object-cover"
        />
        <div>
          <p class="text-xs text-white">
            <strong class="block font-medium">Admin@gmail.com </strong>
          </p>
        </div>
      </a>
      <div
        class="p-2.5 mt-3 flex items-center rounded-md px-4 duration-300 cursor-pointer hover:bg-red-600 text-white"
        onClick={logout}
      >
        <ImExit className="text-xl" />
        <span class="text-[15px] ml-4 text-gray-200 font-bold">Log-Out</span>
      </div>
    </div>
  );
}

import React from "react";
import Sidebar from "./Sidebar";
export default function Dashboard() {
  return (
    <div className="flex overflow-x-hidden">
      <div>
        <Sidebar />
      </div>
      <div className="container mx-auto p-2">
        <div class="grid gap-6 mb-2 mt-2 md:grid-cols-3">
          <div class="block rounded-xl border bg-gray-800 border-gray-800 p-8 shadow-xl transition hover:border-green-500 hover:shadow-green-500">
            <p class="text-md text-center font-medium text-white">
              Daftar Pasien Guru
            </p>
            <div class="text-3xl text-center text-green-400 font-semibold mb-2">
              <span class="fa-stack fa-xs">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-wheelchair fa-stack-1x fa-inverse"></i>
              </span>
              0 Guru
            </div>
          </div>
          <div class="block rounded-xl border bg-gray-800 border-gray-800 p-8 shadow-xl transition hover:border-green-500 hover:shadow-green-500">
            <p class="text-md text-center font-medium text-white">
              Daftar Pasien Siswa
            </p>
            <div class="text-3xl text-center text-green-400 font-semibold mb-2">
              <span class="fa-stack fa-xs">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-wheelchair fa-stack-1x fa-inverse"></i>
              </span>
              0 Siswa
            </div>
          </div>
          <div class="block rounded-xl border bg-gray-800 border-gray-800 p-8 shadow-xl transition hover:border-green-500 hover:shadow-green-500">
            <p class="text-md text-center font-medium text-white">
              Daftar Pasien Karyawan
            </p>
            <div class="text-3xl text-center text-green-400 font-semibold mb-2">
              <span class="fa-stack fa-xs">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-wheelchair fa-stack-1x fa-inverse"></i>
              </span>
              0 Karyawan
            </div>
          </div>
        </div>
        <div className="max-h-screen overflow-y-auto flex-1 p-6 lg:px-2 mt">
          <div class="text-center py-2.5 rounded-t-lg mt-4 bg-green-400 text-white text-xl">
            Riwayat Pasien
          </div>
          <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left text-gray-800 dark:text-gray-400">
              <thead class="text-xs text-amber-900 uppercase bg-grey-600 dark:bg-gray-800 dark:text-gray-800">
                <tr>
                  <th scope="col" class="px-6 py-3">
                    No
                  </th>
                  <th scope="col" class="px-6 py-3">
                    Nama Pasien
                  </th>
                  <th scope="col" class="px-6 py-3">
                    Status Pasien
                  </th>
                  <th scope="col" class="px-6 py-3">
                    Tanggal Lahir
                  </th>
                  <th scope="col" class="px-6 py-3">
                    <span class="sr-only">Edit</span>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                  <th
                    scope="row"
                    class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                  >
                    1
                  </th>
                  <td class="px-6 py-4">Yanto Supardi</td>
                  <td class="px-6 py-4">SISWA</td>
                  <td class="px-6 py-4">26 september (00.38)</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

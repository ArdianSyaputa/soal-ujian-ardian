import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [passwordType, setPasswordType] = useState("password");
  const [passwordIcon ,setPasswordIcon] = useState("fa-solid fa-eye-slash")

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      setPasswordIcon("fa-solid fa-eye")
      return;
    }
    setPasswordType("password");
    setPasswordIcon("fa-solid fa-eye-slash")
  };

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:2005/sekolah/login",
        {
          email: email,
          password: password,
        }
      );
      // Jika respon 200/ ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil!!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.user.id);
        localStorage.setItem("role", data.data.user.role);    
        history.push("/home");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  return (
    <div className="mx-auto md:w-[40%] sm:px-6">
       <h1 className="text-center text-2xl font-bold text-green-600 hover:text-green-800 sm:text-3xl">
       SISTEM APLIKASI UKS SMK BINANUSANTARA
        </h1> 
      <div className="ml-44"> 
      <img width={150} src="https://www.zarla.com/images/zarla-aphotez-1x1-2400x2400-20220823-pm48gq6df93rd6hx96tb.png?crop=1:1,smart&width=250&dpr=2" alt="" />
      </div>
     <div className="mx-auto">
          
        <form
          onSubmit={login}
          action=""
          className="mt-6 mb-0 space-y-4 rounded-lg p-4 shadow-lg border sm:p-6 lg:p-8"
        >
        <h1 className="text-center text-2xl font-bold text-black-200 hover:text-green-800 sm:text-3xl">
          Login
        </h1>    
          <div>
            <label className="sr-only">
              Username / Email
            </label>
            <div className="relative">
              <input
                type="text"
                className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                placeholder="Enter email or username"
                onChange={(e) => setEmail(e.target.value)}
                required
              />

              <span className="absolute inset-y-0 right-0 grid place-content-center px-4">
                <i className="fa-light fa-at text-lg font-semibold"></i>
              </span>
            </div>
          </div>

          <div>
          <label htmlFor="password" className="sr-only">
            Password
          </label>

          <div className="relative">
            <input
              value={password}
              type={passwordType}
              className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
              placeholder="Enter password"
              onChange={(e) => setPassword(e.target.value)}
            />

            <span
              onClick={togglePassword}
              className="absolute inset-y-0 right-0 grid place-content-center px-4"
            >
          <i class={passwordIcon}></i>
            </span>
          </div>
        </div>

          <button
            type="submit"
            className="block w-full rounded-lg bg-green-600 hover:bg-green-800 px-5 py-3 text-sm font-medium text-white"
          >
            Login
          </button>
          <p class="text-center text-sm text-gray-500">
        No account?
        <a class="underline" href="/Register">Sign up</a>
      </p>
        </form>
        
      </div>
    </div>

  );
}

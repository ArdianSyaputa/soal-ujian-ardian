import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Login from "./Cmponent/Login";
import Dashboard from "./Cmponent/Dashboard";
import Register from "./Cmponent/Register";
import "./App.css";
import PeriksaPasien from "./Page/PeriksaPasien";
import DaftarSiswa from "./PageData/DaftarSiswa";
import DaftarGuru from "./PageData/DaftarGuru";
import DaftarKaryawan from "./PageData/DaftarKaryawan";
import Penanganan from "./Page/PenanganPertama";
import Tindakan from "./Page/Tindakan";
import Diagnosa from "./Page/Diagnosa";
import DaftarObat from "./Page/DaftarObat";
import Home from "./Cmponent/Home";
import EditGuru from "./Page Edit/EditGuru";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/Dashboard" component={Dashboard} exact />
          <Route path="/Home" component={Home} exact />
          <Route path="/DaftarObat" component={DaftarObat} exact />
          <Route path="/Penanganan" component={Penanganan} exact />
          <Route path="/Diagnosa" component={Diagnosa} exact />
          <Route path="/Tindakan" component={Tindakan} exact />
          <Route path="/EditGuru" component={EditGuru} exact />

          <Route path="/DaftarSiswa" component={DaftarSiswa} exact />
          <Route path="/DaftarGuru" component={DaftarGuru} exact />
          <Route path="/DaftarKaryawan" component={DaftarKaryawan} exact />
          <Route path="/Periksa" component={PeriksaPasien} exact />
          <Route path="/" component={Login} exact />
          <Route path="/register" component={Register} exact />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;

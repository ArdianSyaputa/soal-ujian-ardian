import React from "react";
import Sidebar from "../Cmponent/Sidebar";
export default function DaftarSiswa() {
  return (
    <div className="flex">
      <div>
        <Sidebar />
      </div>
      <div class="container mx-auto  p-2">
        <div class="grid grid-cols-1 px-2 md:grid-cols-3 rounded-t-lg py-2.5 bg-green-400 text-white text-xl">
        <div class="flex justify-center mb-2 md:justify-start md:pl-6">
            Daftar Siswa
          </div>
          <div class="flex flex-wrap justify-center col-span-2 gap-2 md:justify-end">
            <button
              type="button"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            >
              Tambah
            </button>
            <button
              type="button"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            >
              Import Data
            </button>
            <button
              type="button"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            >
              Download Data
            </button>
          </div>
        </div>
        <div class="overflow-x-auto w-full px-4 bg-white rounded-b-lg shadow">
          <table class="my-4 w-full divide-y divide-gray-300 text-center">
            <thead class="bg-gray-50">
              <tr>
                <th class="px-3 py-2 text-xs text-gray-500">NO</th>
                <th class="px-3 py-2 text-xs text-gray-500">NAMA SISWA</th>
                <th class="px-3 py-2 text-xs text-gray-500">KELAS</th>
                <th class="px-3 py-2 text-xs text-gray-500">TANGGAL LAHIR</th>
                <th class="px-3 py-2 text-xs text-gray-500">ALAMAT</th>
                <th class="px-3 py-2 text-xs text-gray-500">AKSI</th>
              </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-300">
              <tr class="whitespace-nowrap">
                <td class="px-3 py-4 text-sm text-gray-500">1</td>
                <td class="px-3 py-4">
                  <div class="text-sm text-gray-900">Yanto Supriadi</div>
                </td>
                <td class="px-3 py-4">
                  <div class="text-sm text-gray-900">XII</div>
                </td>
                <td class="px-3 py-4">
                  <div class="text-sm text-gray-500">2023-05-12</div>
                </td>
                <td class="px-3 py-4 text-sm text-gray-500">
                  <div class="text-sm text-gray-500">jl Penerangan selatan</div>
                </td>
                <td class="flex justify-content-center gap-4 px-3 py-4">
                  <div>
                    <a href="/edit-guru/4">
                      <button>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          class="w-6 h-6 text-blue-400"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"
                          ></path>
                        </svg>
                      </button>
                    </a>
                  </div>
                  <div>
                    <button>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="w-6 h-6 text-red-400"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                          d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                        ></path>
                      </svg>
                    </button>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

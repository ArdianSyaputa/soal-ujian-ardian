import React from "react";

export default function EditGuru() {
  return (
    <div class="">
      <div class="flex justify-content-center container mx-auto pt-6 ml-4 mr-4">
        <div class="w-1/2 px-1 py-5 bg-white rounded-lg shadow">
          <form class="ml-4 mr-4">
            <div class="mb-4 text-center">
              <h1>Edit Siswa</h1>
            </div>
            <div class="name mb-3">
              <label class="form-label">
                <strong>Nama Siswa</strong>
              </label>
              <div class="d-flex gap-3 input-group">
                <input
                  placeholder="Nama Siswa"
                  class="form-control"
                  value="Secondta"
                />
              </div>
            </div>
            <div class="name mb-3">
              <label class="form-label">
                <strong>Kelas</strong>
              </label>
              <select aria-label="kelas" class="form-select">
                <option class="hidden">IX H</option>
                <option>VII A</option>
                <option>VII B</option>
                <option>VII C</option>
                <option>VII D</option>
                <option>VII E</option>
                <option>VII F</option>
                <option>VII G</option>
                <option>VII H</option>
                <option>VII I</option>
                <option>VIII A</option>
                <option>VIII B</option>
                <option>VIII C</option>
                <option>VIII D</option>
                <option>VIII E</option>
                <option>VIII F</option>
                <option>VIII G</option>
                <option>VIII H</option>
                <option>VIII I</option>
                <option>IX A</option>
                <option>IX B</option>
                <option>IX C</option>
                <option>IX D</option>
                <option>IX E</option>
                <option>IX F</option>
                <option>IX G</option>
                <option>IX H</option>
                <option>IX I</option>
              </select>
            </div>
            <div class="name mb-3">
              <label class="form-label">
                <strong>Tempat Lahir</strong>
              </label>
              <div class="d-flex gap-3 input-group">
                <input
                  placeholder="Tempat Lahir"
                  class="form-control"
                  value="Semarang"
                />
              </div>
            </div>
            <div class="name mb-3">
              <label class="form-label">
                <strong>Tanggal Lahir</strong>
              </label>
              <div class="d-flex gap-3 input-group">
                <input type="date" class="form-control" value="2023-05-24" />
              </div>
            </div>
            <div class="name mb-3">
              <label class="form-label">
                <strong>Alamat</strong>
              </label>
              <div class="d-flex gap-3 input-group">
                <input
                  placeholder="Alamat"
                  class="form-control"
                  value="Jl. Semarang"
                />
              </div>
            </div>
            <div class="d-flex justify-content-end align-items-center mt-2">
              <button
                class="bg-red-600
    text-white hover:bg-red-700 
    focus:ring-4 focus:outline-none focus:ring-red-300 
    font-medium rounded-lg text-md w-full sm:w-auto px-4 py-2 text-center"
              >
                Batal
              </button>
              <div class="ms-2 me-2">||</div>
              <button
                class="bg-green-500
    text-white hover:bg-green-600 
    focus:ring-4 focus:outline-none focus:ring-green-300 
    font-medium rounded-lg text-md w-full sm:w-auto px-4 py-2 text-center"
                type="submit"
              >
                Simpan
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

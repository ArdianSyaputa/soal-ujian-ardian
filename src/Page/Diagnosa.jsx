import React, { useState, useEffect } from "react";
import Sidebar from "../Cmponent/Sidebar";
import Swal from "sweetalert2/dist/sweetalert2.js";
import axios from "axios";
import { AiTwotoneDelete } from "react-icons/ai";
import { AiFillEdit } from "react-icons/ai";
import { Modal } from "react-bootstrap";
export default function Diagnosa() {
  const [totalPages, setTotalPages] = useState([]);
  const [search, setSearch] = useState([]);
  const [akun, setAkun] = useState([]);
  const [nama_Diagnosa, setNamaDiagnosa] = useState("");
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [show, setShow] = useState(false);
  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post("http://localhost:2005/diagnosa", {
        nama_Diagnosa: nama_Diagnosa,
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAllDiagnosa = async (page = 0) => {
    await axios
      .get(`http://localhost:2005/diagnosa?page=${page}&search=${search}`)
      .then((res) => {
        setTotalPages(res.data.data.totalPages);
        setSearch(search);
        setAkun(res.data.data.content);
      })
      .catch((error) => {
        console.log("Terjadi Kesalahan " + error);
      });
  };
  useEffect(() => {
    getAllDiagnosa();
  }, []);

  const deleteDiagnosa = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:2005/diagnosa/` + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  return (
    <div className="flex">
      <div>
        <Sidebar />
      </div>
      <div class="container mx-auto  p-2">
        <div class="grid grid-cols-1 px-2 md:grid-cols-3 rounded-t-lg py-2.5 bg-green-400 text-white text-xl">
          <div class="flex justify-center mb-2 md:justify-start md:pl-6">
            Diagnosa Penyakit
          </div>
          <div class="flex flex-wrap justify-center col-span-2 gap-2 md:justify-end">
            <button
              type="button"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
              onClick={handleShow}
            >
              Tambah
            </button>
          </div>
        </div>
        <div class="overflow-x-auto w-full px-4 bg-white rounded-b-lg shadow">
          <table class="my-4 w-full divide-y divide-gray-300 text-center">
            <thead class="bg-gray-50">
              <tr>
                <th class="px-3 py-2 text-xs text-gray-500">NO</th>
                <th class="px-3 py-2 text-xs text-gray-500">NAMA DIAGNOSA</th>
                <th class="px-3 py-2 text-xs text-gray-500">AKSI</th>
              </tr>
            </thead>
            {akun.map((down, idx) => {
              return (
                <tr>
                  <td class="whitespace-nowrap px-4 text-center py-2 font-medium text-gray-900">
                    {idx + 1}
                  </td>
                  <td class="whitespace-nowrap text-center px-4 py-2 text-gray-700 ">
                    {down.nama_Diangnosa}
                  </td>
                  <td class="flex gap-4 px-3 py-4 justify-content-center">
                  <div>
                    <a href="/penanganan/8">
                      <button>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          class="w-6 h-6 text-blue-400"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"
                          ></path>
                        </svg>
                      </button>
                    </a>
                  </div>
                  <div>
                    <button onClick={() => deleteDiagnosa(down.id)} >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="w-6 h-6 text-red-400"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                          d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                        ></path>
                      </svg>
                    </button>
                  </div>
                </td>
                </tr>
              );
            })}
          </table>
        </div>
      </div>

      {/* MODAL DIAGNOSA */}
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                Tambahkan
              </h3>
              <form className="space-y-3" onSubmit={add}>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Status Pasien
                  </label>
                  <input
                    placeholder="Status Pasien"
                    onChange={(e) => setNamaDiagnosa(e.target.value)}
                    value={nama_Diagnosa}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>

                <button
                  onClick={handleClose}
                  type="submit"
                  className="w-full text-black bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Tambah
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    </div>

  );
}

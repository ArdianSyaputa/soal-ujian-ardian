import Sidebar from "../Cmponent/Sidebar";
import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2/dist/sweetalert2.js";
export default function PeriksaPasien() {
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [show, setShow] = useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);
  const [show1, setShow1] = useState(false);
  return (
    <div className="flex">
      <div>
        <Sidebar />
      </div>
      <div class="container mx-auto  p-2">
        <div class="grid grid-cols-1 px-2 md:grid-cols-3 py-2.5 bg-green-400 text-white text-xl rounded-t-lg mt-4">
          <div class="flex justify-center mb-2 md:justify-start md:pl-6">
            Filter Rekap Data
          </div>
          <div class="flex flex-wrap justify-center col-span-2 gap-2 md:justify-end">
            <button
              data-modal-target="authentication-modal"
              data-modal-toggle="authentication-modal"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
              type="submit"
              onClick={handleShow1}
            >
              Filter Tanggal
            </button>
          </div>
        </div>
        <div class="w-full px-4 bg-white rounded-b-lg shadow">
          <div class="grid mb-2 py-6 md:grid-cols-1">
            <div class="flex justify-center my-2">
              <button
                type="button"
                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
              >
                Download Rekap Data Pasien
              </button>
            </div>
          </div>
        </div>
        <div class="grid grid-cols-1 px-2 md:grid-cols-3 py-2.5 bg-green-400 text-white text-xl rounded-t-lg mt-4">
          <div class="flex justify-center mb-2 md:justify-start md:pl-6">
            Daftar Pasien
          </div>
          <div class="flex flex-wrap justify-center col-span-2 gap-2 md:justify-end ">
            <button
              data-modal-target="authentication-modal"
              data-modal-toggle="authentication-modal"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
              type="submit"
              onClick={handleShow}
            >
              <i className="fas fa-plus"></i> Tambah
            </button>
          </div>
        </div>
        <div class="overflow-x-auto w-full px-4 bg-white rounded-b-lg shadow">
          <table class="my-4 w-full divide-y divide-gray-300 text-center">
            <thead class="bg-gray-50">
              <tr>
                <th class="px-2 py-2 text-xs text-gray-500">NO</th>
                <th class="px-2 py-2 text-xs text-gray-500">NAMA PASIEN</th>
                <th class="px-2 py-2 text-xs text-gray-500">STATUS PASIEN</th>
                <th class="px-2 py-2 text-xs text-gray-500">JABATAN</th>
                <th class="px-2 py-2 text-xs text-gray-500">TANGGAL / JAM</th>
                <th class="px-2 py-2 text-xs text-gray-500">KETERANGAN</th>
                <th class="px-2 py-2 text-xs text-gray-500">STATUS</th>
                <th class="px-2 py-2 text-xs text-gray-500">AKSI</th>
              </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-300"></tbody>
          </table>
        </div>
      </div>
      {/* MODAL PERIKSA PASIEN */}
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                Tambahkan
              </h3>
              <form className="space-y-3">
                {/* <form className="space-y-3" onSubmit={add}> */}
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Status Pasien
                  </label>
                  <input
                    placeholder="Status Pasien"
                    // onChange={(e) => setNamaAkun(e.target.value)}
                    // value={namaAkun}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Nama Pasien
                  </label>
                  <input
                    placeholder="Nama Pasien"
                    // onChange={(e) => setNamaAkun(e.target.value)}
                    // value={namaAkun}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Keluhan Pasien
                  </label>
                  <input
                    placeholder="Keluhan Pasien"
                    // onChange={(e) => setJenisAkun(e.target.value)}
                    // value={jenisAkun}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <button
                  onClick={handleClose}
                  type="submit"
                  className="w-full text-black bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Tambah
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>

      {/* MODAL FILTER TANGGAL */}
      <Modal
        show={show1}
        onHide={handleClose1}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose1}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                Tambahkan
              </h3>
              <form className="space-y-3">
                {/* <form className="space-y-3" onSubmit={add}> */}
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Dari Tanggal
                  </label>
                  <input
                    placeholder="Status Pasien"
                    type="date"
                    // onChange={(e) => setNamaAkun(e.target.value)}
                    // value={namaAkun}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Sampai Tanggal
                  </label>
                  <input
                    placeholder="Nama Pasien"
                    type="date"
                    // onChange={(e) => setNamaAkun(e.target.value)}
                    // value={namaAkun}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
               
                <button
                  onClick={handleClose1}
                  type="submit"
                  className="w-full text-black bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Tambah
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
